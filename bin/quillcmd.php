<?php

/**
 * Class QuillCmd
 *
 * This is the frontend that handles command line operations.
 */
class QuillCmd extends mtvee\quill\Cmd
{
    protected $prompt = 'quill>';

    /**
     * This does the work of parsing the command line and executing
     * the command.
     *
     * @param array $args the command line args a la $argv
     * @throws \Exception
     */
    public function engage($args)
    {
        $this->script_name = array_shift($args);

        // need at least one arg
        if (count($args) < 1)
            $this->usage();

        $opts = $this->parse_args($args);

        // check for global switches
        if (array_key_exists('env', $opts)) {
            define('ENVIRONMENT', $opts['env']);
            unset($opts['env']);
        }

        $this->run_one_cmd($opts);
    }

    protected function cmd_status($args)
    {
        if (count($args) == 0) {
            print("build command expects one argument");
            return;
        }
        $quill = new mtvee\quill\Quill;
        try {
            $quill->status($args[0]);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * command: build PATH
     *
     * @param array $args
     */
    protected function cmd_build($args)
    {
        if (count($args) == 0) {
            print("build command expects one argument");
            return;
        }
        $quill = new mtvee\quill\Quill;
        try {
            $quill->run($args[0]);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * This creates a simple site using a couple of templates.
     *
     * @param array $args
     */
    protected function cmd_create($args)
    {
        // don't overwrite
        if (file_exists('data'))
            die("'data' dir already exists!");

        if (file_exists('public'))
            die("'public' dir already exists!");

        print("Creating: ./data\n");
        mkdir('data');
        print("Creating: ./data/.meta\n");
        mkdir('data/.meta');
        print("Creating: ./data/.meta/plugins\n");
        mkdir('data/.meta/plugins');
        print("Creating: ./public\n");
        mkdir('public');

        $config = file_get_contents(__DIR__ . '/../skeleton/.meta/config.json');
        print("Writing: data/.meta/config.json\n");
        file_put_contents('data/.meta/config.json', $config);

        $layout = file_get_contents(__DIR__ . '/../skeleton/.meta/layout.php');
        print("Writing: data/.meta/layout.php\n");
        file_put_contents('data/.meta/layout.php', $layout);

        $plugin = file_get_contents(__DIR__ . '/../skeleton/.meta/plugins/Tester.php');
        print("Writing: data/.meta/plugins/Tester.php\n");
        file_put_contents('data/.meta/plugins/Tester.php', $plugin);

        $index = file_get_contents(__DIR__ . '/../skeleton/index.md');
        print("Writing: data/index.md\n");
        file_put_contents('data/index.md', $index);

        print("All done. Have fun!\n");
    }

    /**
     * Remove a tree of directories
     *
     * @param string $dir the root of the path to remove
     * @return bool True on success
     */
    public function rmdirs($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->rmdirs("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    /**
     * Print out a usage screen
     *
     * @param string $script_name as it appears on the command line ($argv[0])
     */
    protected function usage()
    {
        $doc = <<<DOC
Build a static website

Usage: $this->script_name COMMAND [OPTIONS]

COMMANDS
--------
 build PATH    build the site in the directory PATH
 create        create a new project in current directory
 
OPTIONS
-------
  --env ENVIRONMENT  the environment (default: production)
  
DOC;

        die($doc);
    }
}