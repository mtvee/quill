<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title><?= $site_name ?></title>
    <meta name="description" content="mtvee devember blog">
    <meta name="author" content="mtvee">

    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FONT
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css">

    <!-- Javascript
    -------------------------------------------------- -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <!-- Favicon
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="icon" type="image/png" href="favicon.png">

</head>
<body>

<!-- Primary Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="container">
    <!-- header -->
    <div class="row">
        <div class="column header">
            <h1><?= $site_name ?></h1>
        </div>
    </div>


    <!-- content -->
    <div class="row">
        <!-- main content -->
        <div class="two-thirds column">
            <span class="date"><?= $date ?></span>
            <?= $page_content ?>

            <hr/>

            <?= Tester::apply(); ?>
        </div>
    </div>

    <!-- footer -->
    <div class="row">
        <div class="column footer">
            <?= $copyright ?><br/>
        </div>
    </div>

</div>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>