Quill
=====

Quill is a static website generator written in PHP. It is aimed at
anyone who might want to keep a blog or documentation. The goal was
to make something quick and simple to use. 

Features
--------

- uses markdown for content input
- layout is a php file so anything can go in there
- output is html via Parsedown
- PSR-4 autoloading compliant structure
- unit tested 

Requirements
------------

- PHP >= 5.6
- composer is nice

Install
-------

_The following steps presume you have composer installed somewhere the system can see it._

create a folder to store your project

    $> mkdir FOLDER; cd FOLDER

install quill via composer

    $> composer require mtvee/quill

or

   $> git clone http://gitlab.com/mtvee/quill
   $> cd quill
   $> composer install

let quill create a skeleton project for you

    $> vendor/bin/quill create
    
build the skeleton project in the data directory

    $> vendor/bin/quill build data

the html output is in `public` so we can use php to run a local server
and view the output in a web browser

    $> php -S localhost:8080 -t public/

put `http://localhost:8080` into your browser url and voila!

You can push from `public` to your actual web site to publish.

#### Next steps

* add/edit the site source files in `data`
* tweak the layout in `data/.meta/layout.php`
* add/edit your css and javascript etc in `public`
* push your stuff to some hosting someplace
* $$PROFIT$$


Contribute
----------

- repo is at [gitlab](https://gitlab.com/mtvee/quill)
- Issue tracker is at [gitlab](https://gitlab.com/mtvee/quill/issues)
- Documentation is at [gitlab](https://gitlab.com/mtvee/quill/wikis/home)

License
-------

MIT



