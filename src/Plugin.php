<?php

namespace mtvee\quill;

/**
 * Interface Plugin for a basic plugin system
 *
 * Plugins go in .meta/plugins.
 * Class name must be the same as the filename i.e. class Calendar in Calendar.php
 * init is called with the config array when loaded
 * apply is static and called within layout i.e. <?= Calendar::apply(...args) ?>
 * apply returns a string injected into layout
 *
 * @package mtvee\quill
 */
interface Plugin
{
    function init( &$config );
    static function apply( ...$args );
}