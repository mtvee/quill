<?php

namespace mtvee\quill {

    /**
     * Class Quill.
     *
     * This is a simple, unassuming static site generator.
     *
     * @package mtvee\quill
     */
    class Quill
    {
        /** @var array $conf the configuration  */
        private $conf = array();
        /** @var string $save_dir setInputDir saves this path so we can restore it int he dtor */
        private $save_dir = null;
        /** @var int $last_build  mtime of 'data/.meta/.last_build' */
        private $last_build = 0;
        private $plugins = array();

        /**
         * Quill constructor.
         */
        public function __construct()
        {
            $this->conf = array();

            if( !defined('ENVIRONMENT')) {
                define("ENVIRONMENT", "production");
            }
            printf("ENV: %s\n", ENVIRONMENT);
        }

        /**
         * Quill destructor.
         *
         * Tries to restore the initial working directory
         */
        function __destruct()
        {
            if( $this->save_dir != null ) {
                chdir($this->save_dir);
            }
        }

        /**
         * Attempt to process all the input files in input_dir and write them
         *
         * @param string[]|string $config configuration params. If an array it expects at least
         *                                one element with the key 'input_dir' which is
         *                                the input files location. If a string in treats
         *                                that as the input files location.
         * @throws \Exception
         */
        public function run( $config )
        {
            if( is_array($config) ) {
                $this->conf = array_merge($this->conf, $config);
            }
            else if( is_string($config)) {
                $this->conf['input_dir'] = $config;
            }
            else {
                throw new \Exception('Bad argument for $config');
            }

            $this->setInputDir($this->conf['input_dir']);

            $this->processDir('.');

            // store the last build time
            // this file can be just nuked for a full build
            file_put_contents('.meta/.last_build', "");

        }

        /**
         * @param $config
         * @throws \Exception
         */
        public function status( $config )
        {
            if( is_array($config) ) {
                $this->conf = array_merge($this->conf, $config);
            }
            else if( is_string($config)) {
                $this->conf['input_dir'] = $config;
            }
            else {
                throw new \Exception('Bad argument for $config');
            }

            $this->setInputDir($this->conf['input_dir']);

        }

        /**
         * Processes a directory of input files and writes to output.
         *
         * @param string $path the directory path to process
         */
        public function processDir( $path )
        {
            $files = array_diff(scandir($path), array('.', '..', '.meta'));
            foreach( $files as $file ) {
                $filename = $path . '/' . $file;
                if( is_dir($filename)) {
                    printf("Scanning: %s\n", $filename);
                    // process a directory
                    $opath = $this->conf['output_dir'] . '/' . $file;
                    if(!file_exists($opath)) {
                        mkdir($opath);
                    }
                    // save our output path so we can restore it
                    $save_output = $this->conf['output_dir'];
                    $this->conf['output_dir'] = $this->conf['output_dir'] . '/' . $file;
                    $this->processDir($filename);
                    $this->conf['output_dir'] = $save_output;
                } else {
                    // process a file
                    $info = pathinfo( $filename );
                    if(filemtime($filename) > $this->last_build) {
                        // markdown
                        if ($info['extension'] == 'md') {
                            printf("Processing: %s\n", $filename);
                            // see if there is some page data
                            $page_meta = $path . '/' . $info['filename'] . '.json';
                            $page_data = null;
                            if (file_exists($page_meta)) {
                                $page_data = json_decode(file_get_contents($page_meta), True);
                            }
                            $page_data['filename'] = $file;
                            $contents = $this->fetchPage($filename, $page_data);
                            $opath = $this->conf['output_dir'] . '/' . $info['filename'] . '.html';
                            file_put_contents($opath, $contents);
                        } else {
                            // straight copy
                            printf("Copying: %s\n", $filename);
                            copy($filename, $this->conf['output_dir'] . '/' . $file);
                        }
                    }
                }
            }
        }

        /**
         * Set the input directory.
         *
         * @param string $inputDir the path to use and input location
         * @throws \Exception
         */
        public function setInputDir( $inputDir )
        {
            // save or current do so we can get back there when we destruct
            $this->save_dir = getcwd();
            // change to the input dir and work out of there
            if(!file_exists($inputDir) || !is_dir($inputDir)) {
                throw new \Exception("path does not exist: $inputDir");
            }
            chdir($inputDir);
            $this->loadConfig('.meta/config.json');
            $this->checkConfig();
            $this->getMeta();
        }

        /**
         * Check for a valid config and that paths exist.
         *
         * @throws \Exception
         */
        public function checkConfig()
        {
            if( !array_key_exists('output_dir', $this->conf)) {
                throw new \Exception("'output_dir' missing in config");
            }

            // see if it is an absolute path and if not assume it
            // is relative to the input_dir (curdir)
            if($this->conf['output_dir'][0] != '/') {
                $this->conf['output_dir'] =  './' . $this->conf['output_dir'];
            }

            if(!file_exists($this->conf['output_dir'])) {
                throw new \Exception("'output_dir' missing or not readable");
            }
        }

        /**
         * Process the meta directory.
         *
         * Processes files in the input directory, transforming markdown
         * to a key->value pair in $this->conf. The key is the filename
         * (without extension) and the value is the processed file.
         */
        public function getMeta()
        {
            $parsedown = new \Parsedown();
            $path =  '.meta';
            $files = array_diff(scandir($path), array('.', '..', $this->conf['layout']));
            foreach( $files as $file ) {
                if(is_dir($path . '/'. $file)) {
                    continue;
                }
                $info = pathinfo( $path . '/' . $file );
                $content = file_get_contents($path . '/' . $file );
                if($info['extension'] == 'md') {
                    $this->conf[$info['filename']] = $parsedown->text($content);
                }
                // check for the last build time
                else if($info['extension'] == 'last_build') {
                    $this->last_build = filemtime($path . '/.last_build');
                }
                else {
                    $this->conf[$info['filename']] = $content;
                }
            }
            // if anything in this dir was touched since the last build we
            // probably need to fully rebuild the site
            if( filemtime($path) > $this->last_build ) {
                $this->last_build = 0;
            }

            $this->loadPlugins( '.meta/plugins');
        }

        protected function loadPlugins( $path )
        {
            $files = array_diff(scandir($path), array('.', '..'));
            foreach( $files as $file ) {
                $info = pathinfo( $path . '/' . $file );
                if($info['extension'] == 'php') {
                    require_once($path . '/' . $file);
                    $className = $info['filename'];
                    $obj = new $className;
                    $obj->init($this->conf);
                    $this->plugins[$className] = $obj;
                }
            }
        }

        /**
         * Process a page and return the output as a string.
         *
         * @param $pageName
         * @return string
         */
        public function fetchPage( $pageName, $userData = null )
        {
            ob_start();
            $this->renderPage($pageName, $userData);
            $output = ob_get_clean();
            return $output;
        }

        /**
         * Process a page and emit the output.
         *
         * @param $pageName
         * @param null $userData
         */
        public function renderPage( $pageName, $userData = null )
        {
            $data = array();

            $template = '.meta/' . $this->conf['layout'];
            $parsedown = new \Parsedown();

            $filename = $pageName;

            $data['date'] = date ("F d Y", filemtime($filename));
            $data = array_merge($data, $this->conf);
            if(is_array($userData)) {
                $data = array_merge($data, $userData);
            }

            $contents = file_get_contents( $filename);
            $data['page_content'] = $parsedown->text($this->preProcess($contents, $data));

            extract( $data );
            include $template;
        }

        public function preProcess( $contents, $data )
        {
            preg_match_all("/\{\{(\s*\w+\s*)\}\}/", $contents, $matches);
            for( $x = 0; $x < count($matches[0]); $x++) {
                if (array_key_exists(trim($matches[1][$x]), $data)) {
                    $contents = str_replace($matches[0][$x], $data[trim($matches[1][$x])], $contents);
                } else {
                    $contents = str_replace($matches[0][$x], "blepslkd", $contents);
            }
            }
            return $contents;
        }

        /**
         * Load the config.json file.
         *
         * @param string $confFile the path to the file
         * @return bool True if successful
         */
        public function loadConfig( $confFile )
        {
            if(!file_exists($confFile)) {
                return False;
            }

            $json = json_decode(file_get_contents($confFile), True);
            if( $json == NULL) {
                return False;
            }
            $this->conf = array_merge($this->conf, $json);
            return True;
        }

    }
}