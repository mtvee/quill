<?php

namespace mtvee\quill;

class Cmd
{
    protected $prompt = 'cmd>';
    protected $script_name = 'cmd';

    public function __construct()
    {
    }

    /**
     * The command loop
     *
     * @return void
     */
    public function loop()
    {
        $handle = NULL;
        if (!function_exists('readline')) {
            $handle = fopen("php://stdin", "r");
        }
        $done = False;
        while (!$done) {
            if ($handle != NULL) {
                printf($this->prompt . ' ');
                $inp = trim(fgets($handle));
            } else {
                $inp = trim(readline($this->prompt . ' '));
            }
            if($this->run_one_cmd($inp)) {
                $done = True;
            }
        }
        if ($handle != NULL) {
            fclose($handle);
        }
    }

    /**
     * Run one command
     *
     * @param string | array $inp the command to parse
     * @return bool
     */
    public function run_one_cmd($inp)
    {
        $tokens = array();

        if (is_array($inp)) {
            if(array_values($inp) !== $inp) {
                // assoc array so we need to flatten it
                array_walk($inp, function($val, $key) use (&$tokens) {
                    $tokens[] = $key;
                    if($val !== '')
                        $tokens[] = $val;
                });
            } else {
                $tokens = $inp;
            }
        } else {
            $tokens = preg_split("/[\s]+/", $inp);
        }

        $cmd = array_shift($tokens);
        $args = $tokens;
        $done = False;


        if ($cmd == NULL) {
            return $done;
        }

        // php sends nothing in the call_user_func if args is empty
        // and causes bitching
        if (count($args) == 0) {
            array_push($args, NULL);
        }

        // see if there is something to run
        if (!method_exists($this, "cmd_$cmd")) {
            if (!$this->run_task($cmd, $args)) {
                printf("command/task not found: %s\n", $cmd);
            }
        } else {
            $done = call_user_func_array(array($this, "cmd_$cmd"), array($args));
        }

        return $done;
    }

    /**
     * Parse an array into an assoc array.
     *
     * This is oriented to parsing $argv style array. Flags are indicated
     * with dashes and optional arguments follow. Non flagged items have
     * a value of '' in the returned assoc and flagged items without
     * an argument are returned with a value of True in the assoc.
     *
     * @param array $args a flat array of values
     * @return array an assoc array with flags and non flags as keys
     */
    public function parse_args( array $args)
    {
        // parse into the opts array
        $opts = array();
        for( $i = 0; $i < count($args); ) {
            $key = $args[$i];
            if( $key[0] == '-') {
                // get rid of the dashes
                while($key[0] == '-') { $key = substr($key, 1); }
                // see if this has a following argument
                if( $i+1 < count($args) && $args[$i+1][0] != '-') {
                    $opts[$key] = $args[$i+1];
                    $i++;
                } else {
                    $opts[$key] = True;
                }
            } else {
                $opts[$key] = '';
            }
            $i++;
        }

        return $opts;
    }

    /**
     * Try to load and run a task
     *
     * @param string $cmd the task name
     * @param array $args the task arguments
     * @return bool True on success
     */
    protected function run_task( $cmd, array $args )
    {
        return False;
    }
}

