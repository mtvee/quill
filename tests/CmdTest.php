<?php


class CmdTest extends PHPUnit_Framework_TestCase
{
    public function testIsThereAnySyntaxError()
    {
        $var = new \mtvee\quill\Cmd;
        $this->assertTrue(is_object($var));
        unset($var);
    }

    public function testArgParsing()
    {
        $var = new \mtvee\quill\Cmd;
        $args = array('script1', 'arg1','--arg2','--arg3','arg3 value');
        $opts = $var->parse_args($args);
        // arg1 is not a flag so it's value should be ''
        $this->assertTrue($opts['arg1'] == '');
        // arg2 is a flag so it should be True
        $this->assertTrue($opts['arg2'] == True);
        //
        $this->assertTrue($opts['arg3'] == 'arg3 value');

        unset($var);
    }

}