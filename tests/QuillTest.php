<?php


class QuillTest extends PHPUnit_Framework_TestCase
{
    public $data_dir;

    protected function setUp()
    {
        if( !defined('ENVIRONMENT')) {
            //define('ENVIRONMENT', 'testing');
        }
        $this->data_dir = 'data';
    }

    public function testIsThereAnySyntaxError()
    {
        $var = new \mtvee\quill\Quill;
        $this->assertTrue(is_object($var));
        unset($var);
    }

    public function testSetInputDir()
    {
        $var = new \mtvee\quill\Quill;
        $var->setInputDir($this->data_dir);
        unset($var);
    }

    /* these two tests are for the personal blog and will blow up with the distro
    public function testRender()
    {
        $var = new \mtvee\quill\Quill;
        $var->setInputDir($this->data_dir);
        $page = $var->fetchPage('index.md', array('foo' =>'Foobar', 'bar' => 'Barfoo'));
        $this->assertEquals('<!DOCTYPE html>', substr($page, 0, 15) );
        $this->assertTrue(strpos($page, 'Foobar') != False );
        //$this->assertTrue(strpos($page,"Tester Plugin Here!") !== False);

        unset($var);
    }

    public function testRun()
    {
        $var = new \mtvee\quill\Quill;
        $var->run($this->data_dir);

        // maje sure things copied over properly
        //TODO make this more generic
        $path = '../public/blog/2017';
        $path = '../public/blog/2017/12';
        $this->assertTrue(file_exists($path) && is_dir($path));
        unset($var);
    }
    */

    public function testQuillCmd()
    {
        // this flags the 'bin/quill' not to run anything
        define("PHPUNIT_TESTING", True);
        //print(getcwd());
        // TODO can put this in the src dir but need to check the paths
        require('bin/quillcmd.php');
        $qcmd = new QuillCmd();
        $this->assertTrue(is_object($qcmd));
        $this->assertTrue(!$qcmd->engage(array('quill', 'bogus-command','--verbose')));

        $test_dir = __DIR__ . '/site_test';
        if( file_exists($test_dir)) {
            $qcmd->rmdirs($test_dir);
        }
        mkdir($test_dir);
        chdir($test_dir);
        $qcmd->engage(array('quill','create'));

        $this->assertTrue(file_exists(  'data/') && is_dir( 'data/'));
        $this->assertTrue(file_exists(  'data/.meta') && is_dir( 'data/.meta'));
        $this->assertTrue(file_exists(  'data/.meta/config.json') );
        $this->assertTrue(file_exists(  'data/.meta/layout.php') );
        $this->assertTrue(file_exists(  'data/.meta/plugins/Tester.php') );
        $this->assertTrue(file_exists(  'data/index.md') );

        $qcmd->engage(array('quill','build','data'));
        $this->assertTrue(file_exists(  'public/index.html') );

        $contents = file_get_contents('public/index.html');
        $this->assertTrue(strpos($contents,"Tester Plugin Here!") !== False);

        $qcmd->rmdirs($test_dir);

        unset($qcmd);

    }
}